# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Martin Schlander <mschlander@opensuse.org>, 2010, 2013.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-08-15 02:21+0200\n"
"PO-Revision-Date: 2013-11-27 19:49+0100\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: imapwizard.es:10
msgid "Personal Settings"
msgstr "Personlige indstillinger"

#. i18n: ectx: property (text), widget (QLabel, label)
#: imapwizard.ui:20
#, kde-format
msgid "Username:"
msgstr "Brugernavn:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: imapwizard.ui:33
#, kde-format
msgid "&Incoming server:"
msgstr "Server til &indkommende:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: imapwizard.ui:46
#, kde-format
msgid "&Outgoing server:"
msgstr "Server til &udgående:"

#. i18n: ectx: property (text), widget (QCheckBox, disconnectedMode)
#: imapwizard.ui:63
#, kde-format
msgid "&Download all messages for offline use"
msgstr "&Download alle breve til offline-brug"
