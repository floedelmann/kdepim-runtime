# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Stelios <sstavra@gmail.com>, 2011.
# Dimitrios Glentadakis <dglent@free.fr>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: accountwizard_maildir\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-08-15 02:21+0200\n"
"PO-Revision-Date: 2014-02-28 11:01+0100\n"
"Last-Translator: Dimitrios Glentadakis <dglent@free.fr>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.5\n"

#: maildirwizard.es:7
msgid "Personal Settings"
msgstr "Προσωπικές ρυθμίσεις"

#. i18n: ectx: property (text), widget (QLabel, label)
#: maildirwizard.ui:19
#, kde-format
msgid "URL:"
msgstr "URL:"
