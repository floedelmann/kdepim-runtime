# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Dimitris Kardarakos <dimkard@gmail.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-08-15 02:21+0200\n"
"PO-Revision-Date: 2014-11-13 19:57+0200\n"
"Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.5\n"

#: vcardwizard.es.cmake:7
msgid "Settings"
msgstr "Ρυθμίσεις"

#: vcardwizard.es.cmake:24
msgid "Default Contact"
msgstr "Προκαθορισμένη επαφή"

#. i18n: ectx: property (text), widget (QLabel, label)
#: vcardwizard.ui:19
#, kde-format
msgid "Filename:"
msgstr "Όνομα αρχείου:"
