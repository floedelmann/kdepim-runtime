# Translation for akonadi_newmailnotifier_agent.po to Euskara/Basque (eu).
# Copyright (C) 2022 This file is copyright:
# This file is distributed under the same license as the kdepim-runtime package.
# KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>.
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kdepim-runtime\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-13 00:46+0000\n"
"PO-Revision-Date: 2022-08-29 23:01+0200\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.04.3\n"

#: newmailnotifieragent.cpp:339
#, kde-format
msgctxt ""
"%2 = name of mail folder; %3 = name of Akonadi POP3/IMAP/etc resource (as "
"user named it)"
msgid "One new email in %2 from \"%3\""
msgid_plural "%1 new emails in %2 from \"%3\""
msgstr[0] "\"%3\"(r)en posta berri bat %2(e)an"
msgstr[1] "\"%3\"(r)en %1 posta berri %2(e)an"

#: newmailnotifieragent.cpp:359
#, kde-format
msgid "New mail arrived"
msgstr "Posta berria iritsi da"

#: newmailnotifieragentsettings.kcfg:34
#, kde-format
msgctxt "%f is a variable for agent. Do not change it"
msgid "A message was received from %f"
msgstr "%f(r)en mezu bat jaso da"

#: newmailnotifierreplymessagejob.cpp:38 newmailnotifiershowmessagejob.cpp:38
#, kde-format
msgid "Unable to start KMail application."
msgstr "Ez da KMail aplikazioa hasteko gai."

#: newmailnotifierselectcollectionwidget.cpp:94
#, kde-format
msgid "Select which folders to monitor for new message notifications:"
msgstr "Hautatu mezu berrien jakinarazpenetarako zein karpeta gainbegiratu:"

#: newmailnotifierselectcollectionwidget.cpp:125
#, kde-format
msgid "Search..."
msgstr "Bilatu..."

#: newmailnotifierselectcollectionwidget.cpp:140
#, kde-format
msgid "&Select All"
msgstr "&Hautatu dena"

#: newmailnotifierselectcollectionwidget.cpp:144
#, kde-format
msgid "&Unselect All"
msgstr "&Desautatu dena"

#: newmailnotifiersettingswidget.cpp:31
#, c-format
msgid ""
"<qt><p>Here you can define message. You can use:</p><ul><li>%s set subject</"
"li><li>%f set from</li></ul></qt>"
msgstr ""
"<qt><p>Hemen mezua definitu dezakezu. Erabil dezakezu:</p><ul><li>%s ezarri "
"gaia</li><li>%f ezarri nork</li></ul></qt>"

#: newmailnotifiersettingswidget.cpp:53
#, kde-format
msgid "Choose which fields to show:"
msgstr "Hautatu erakutsi beharreko eremuak:"

#: newmailnotifiersettingswidget.cpp:58
#, kde-format
msgid "Show Photo"
msgstr "Erakutsi argazkia"

#: newmailnotifiersettingswidget.cpp:62
#, kde-format
msgid "Show From"
msgstr "Erakutsi Nork"

#: newmailnotifiersettingswidget.cpp:66
#, kde-format
msgid "Show Subject"
msgstr "Erakutsi Gaia"

#: newmailnotifiersettingswidget.cpp:70
#, kde-format
msgid "Show Folders"
msgstr "Erakutsi Karpetak"

#: newmailnotifiersettingswidget.cpp:74
#, kde-format
msgid "Do not notify when email was sent by me"
msgstr "Ez jakinarazi e-posta nik bidali dudanean"

#: newmailnotifiersettingswidget.cpp:78
#, kde-format
msgid "Keep Persistent Notification"
msgstr "Mantendu jakinarazpen iraunkorra"

#: newmailnotifiersettingswidget.cpp:82
#, kde-format
msgid "Show Action Buttons"
msgstr "Erakutsi ekintza-botoiak"

#: newmailnotifiersettingswidget.cpp:90
#, kde-format
msgid "Reply Mail"
msgstr "Erantzun postari"

#: newmailnotifiersettingswidget.cpp:98 specialnotifierjob.cpp:154
#, kde-format
msgid "Reply to Author"
msgstr "Erantzun egileari"

#: newmailnotifiersettingswidget.cpp:98 specialnotifierjob.cpp:157
#, kde-format
msgid "Reply to All"
msgstr "Erantzun denei"

#: newmailnotifiersettingswidget.cpp:107
#, kde-format
msgid "Display"
msgstr "Azaldu"

#: newmailnotifiersettingswidget.cpp:112
#, kde-format
msgid "Enabled"
msgstr "Gaituta"

#: newmailnotifiersettingswidget.cpp:116
#, kde-format
msgid "<a href=\"whatsthis\">How does this work?</a>"
msgstr "<a href=\"whatsthis\">Hori nola dabil?</a>"

#: newmailnotifiersettingswidget.cpp:125
#, kde-format
msgid "Message:"
msgstr "Mezua:"

#: newmailnotifiersettingswidget.cpp:136
#, kde-format
msgid "Text to Speak"
msgstr "Esan beharreko testua"

#: newmailnotifiersettingswidget.cpp:142
#, kde-format
msgid "Notify"
msgstr "Jakinarazi"

#: newmailnotifiersettingswidget.cpp:145
#, kde-format
msgid "Folders"
msgstr "Karpetak"

#: newmailnotifiersettingswidget.cpp:148
#, kde-format
msgid "New Mail Notifier Agent"
msgstr "Posta berriak jakinarazteko agentea"

#: newmailnotifiersettingswidget.cpp:150
#, kde-format
msgid "Notify about new mails."
msgstr "Posta berrien berri eman."

#: newmailnotifiersettingswidget.cpp:152
#, kde-format
msgid "Copyright (C) 2013-%1 Laurent Montel"
msgstr "Copyright (C) 2013-%1 Laurent Montel"

#: newmailnotifiersettingswidget.cpp:155
#, kde-format
msgid "Laurent Montel"
msgstr "Laurent Montel"

#: newmailnotifiersettingswidget.cpp:155
#, kde-format
msgid "Maintainer"
msgstr "Mantentzailea"

#: newmailnotifiersettingswidget.cpp:156
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Iñigo Salvador Azurmendi"

#: newmailnotifiersettingswidget.cpp:156
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "xalba@ni.eus"

#: specialnotifierjob.cpp:116
#, kde-format
msgid "From: %1"
msgstr "Nork: %1"

#: specialnotifierjob.cpp:124
#, kde-format
msgid "Subject: %1"
msgstr "Gaia: %1"

#: specialnotifierjob.cpp:127
#, kde-format
msgid "In: %1"
msgstr "Non: %1"

#: specialnotifierjob.cpp:150
#, kde-format
msgid "Show mail..."
msgstr "Erakutsi posta..."

#: specialnotifierjob.cpp:150
#, kde-format
msgid "Mark As Read"
msgstr "Markatu irakurritako gisa"

#: specialnotifierjob.cpp:150
#, kde-format
msgid "Delete"
msgstr "Ezabatu"
