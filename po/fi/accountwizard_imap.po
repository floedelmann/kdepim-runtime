# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Tommi Nieminen <translator@legisign.org>, 2011.
# Lasse Liehu <lasse.liehu@gmail.com>, 2013.
#
# KDE Finnish translation sprint participants:
msgid ""
msgstr ""
"Project-Id-Version: accountwizard_imap\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-08-15 02:21+0200\n"
"PO-Revision-Date: 2013-11-16 00:39+0200\n"
"Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>\n"
"Language-Team: Finnish <lokalisointi@lists.coss.fi>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-POT-Import-Date: 2012-12-01 22:24:46+0000\n"
"X-Generator: Lokalize 1.5\n"

#: imapwizard.es:10
msgid "Personal Settings"
msgstr "Henkilökohtaiset asetukset"

#. i18n: ectx: property (text), widget (QLabel, label)
#: imapwizard.ui:20
#, kde-format
msgid "Username:"
msgstr "Käyttäjätunnus:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: imapwizard.ui:33
#, kde-format
msgid "&Incoming server:"
msgstr "&Saapuvan postin palvelin:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: imapwizard.ui:46
#, kde-format
msgid "&Outgoing server:"
msgstr "&Lähtevän postin palvelin:"

#. i18n: ectx: property (text), widget (QCheckBox, disconnectedMode)
#: imapwizard.ui:63
#, kde-format
msgid "&Download all messages for offline use"
msgstr "&Nouda kaikki viestit käytettäviksi verkotta"
