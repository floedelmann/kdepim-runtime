# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Tommi Nieminen <translator@legisign.org>, 2010, 2011, 2013.
# Lasse Liehu <lasse.liehu@gmail.com>, 2012, 2013.
#
# KDE Finnish translation sprint participants:
# Author: Lliehu
msgid ""
msgstr ""
"Project-Id-Version: akonadi_maildispatcher_agent\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-13 00:46+0000\n"
"PO-Revision-Date: 2013-12-22 14:00+0200\n"
"Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>\n"
"Language-Team: Finnish <lokalisointi@lists.coss.fi>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-POT-Import-Date: 2012-12-01 22:24:48+0000\n"
"X-Generator: Lokalize 1.5\n"

#: maildispatcheragent.cpp:84 maildispatcheragent.cpp:233
#, kde-format
msgid "Sending messages (1 item in queue)..."
msgid_plural "Sending messages (%1 items in queue)..."
msgstr[0] "Lähetetään viestejä (1 tietue jonossa)…"
msgstr[1] "Lähetetään viestejä (%1 tietuetta jonossa)…"

#: maildispatcheragent.cpp:94
#, kde-format
msgid "Sending canceled."
msgstr "Lähetys peruttu."

#: maildispatcheragent.cpp:101
#, kde-format
msgid "Finished sending messages."
msgstr "Viestien lähetys valmis."

#: maildispatcheragent.cpp:107
#, kde-format
msgctxt "Notification title when email was sent"
msgid "E-Mail Successfully Sent"
msgstr "Sähköpostin lähetys onnistui"

#: maildispatcheragent.cpp:108
#, kde-format
msgctxt "Notification when the email was sent"
msgid "Your E-Mail has been sent successfully."
msgstr "Sähköpostisi lähetys onnistui."

#: maildispatcheragent.cpp:114
#, kde-format
msgid "No items in queue."
msgstr "Ei tietueita jonossa."

#: maildispatcheragent.cpp:164
#, kde-format
msgid "Online, sending messages in queue."
msgstr "Verkkoyhteys: lähetetään jonossa olevia viestejä."

#: maildispatcheragent.cpp:168
#, kde-format
msgid "Offline, message sending suspended."
msgstr "Ei verkkoyhteyttä: viestien lähetys odottaa."

#: maildispatcheragent.cpp:193
#, kde-format
msgctxt "Message with given subject is being sent."
msgid "Sending: %1"
msgstr "Lähetetään: %1"

#: maildispatcheragent.cpp:258
#, kde-format
msgctxt "Notification title when email sending failed"
msgid "E-Mail Sending Failed"
msgstr "Sähköpostin lähetys epäonnistui"

#: maildispatcheragent.cpp:291
#, kde-format
msgid "Ready to dispatch messages."
msgstr "Valmiina välittämään viestejä."

#. i18n: ectx: label, entry (Outbox), group (General)
#: maildispatcheragent.kcfg:10
#, kde-format
msgid "Outbox collection id"
msgstr "Lähtevien kansion kokoelmatunniste"

#. i18n: ectx: label, entry (SentMail), group (General)
#: maildispatcheragent.kcfg:14
#, kde-format
msgid "Sent Mail collection id"
msgstr "Lähetettyjen viestin kokoelman tunniste"

#: outboxqueue.cpp:265
#, kde-format
msgid "Could not access the outbox folder (%1)."
msgstr "Lähtevän postin kansioon (%1) ei päästä käsiksi."

#: sendjob.cpp:49 sendjob.cpp:50
#, kde-format
msgid "Message sending aborted."
msgstr "Viestin lähetys keskeytettiin."

#: sendjob.cpp:59
#, kde-format
msgid "Could not initiate message transport. Possibly invalid transport."
msgstr ""
"Viestinvälitystä ei saatu alustetuksi: todennäköisesti virheellinen välitys."

#: sendjob.cpp:65
#, kde-format
msgid "Could not send message. Invalid transport."
msgstr "Viestiä ei voitu lähettää. Virheellinen välitys."

#: sendjob.cpp:97
#, kde-format
msgid "Failed to get D-Bus interface of resource %1."
msgstr "Ei saatu D-Bus-liitäntää resurssille %1."

#: sendjob.cpp:110
#, kde-format
msgid "Invalid D-Bus reply from resource %1."
msgstr "Virheellinen D-Bus-vastaus resurssilta %1."

#: sendjob.cpp:228
#, kde-format
msgid "Default sent-mail folder unavailable. Keeping message in outbox."
msgstr ""
"Lähetetyn postin oletuskansio ei ole saatavilla. Pidetään viesti lähtevien "
"laatikossa."

#: sendjob.cpp:240
#, kde-format
msgid "Message transport aborted."
msgstr "Viestin välitys keskeytettiin."

#: sendjob.cpp:240
#, kde-format
msgid "Failed to transport message."
msgstr "Viestiä ei voitu välittää."

#: sendjob.cpp:282
#, kde-format
msgid "Failed to get D-Bus interface of mailfilteragent."
msgstr "Ei saatu postien suodatusagentin D-Bus-liitäntää."

#: sendjob.cpp:289
#, kde-format
msgid "Invalid D-Bus reply from mailfilteragent"
msgstr "Virheellinen D-Bus-vastaus postien suodatusagentilta."

#: sendjob.cpp:331
#, kde-format
msgid "Sending succeeded, but failed to remove the message from the outbox."
msgstr "Lähetys onnistui, mutta viestin poistaminen lähtevistä epäonnistui."

#: sendjob.cpp:334
#, kde-format
msgid ""
"Sending succeeded, but failed to move the message to the sent-mail folder."
msgstr "Lähetys onnistui, mutta viestin siirtäminen lähetettyihin epäonnistui."

#: sendjob.cpp:370
#, kde-format
msgid "Failed to store result in item."
msgstr "Tulosta ei voitu tallentaa tietueeseen."

#: storeresultjob.cpp:60
#, kde-format
msgid "Failed to fetch item."
msgstr "Tietueen nouto epäonnistui."

#~ msgid "Mail Dispatcher Agent Settings"
#~ msgstr "Viestinvälitysagentin asetukset"

#~ msgid "Select the collection to be used as outbox:"
#~ msgstr "Valitse lähtevien laatikkona käytettävä kokoelma:"

#~ msgid "Select the collection to move sent messages into:"
#~ msgstr "Valitse kokoelma, johon lähetetyt viestit siirretään:"

#, fuzzy
#~| msgid "Sending canceled."
#~ msgid "Sending succeeded, %1."
#~ msgstr "Lähetys peruutettu."
