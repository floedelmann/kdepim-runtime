# Copyright © 2010, 2011, 2012 This_file_is_part_of_KDE
# This file is distributed under the same license as the kdepim-runtime package.
# Tommi Nieminen <translator@legisign.org>, 2010, 2011, 2012, 2013, 2016, 2017, 2018, 2021.
# Lasse Liehu <lasse.liehu@gmail.com>, 2012, 2013, 2014.
#
# KDE Finnish translation sprint participants:
msgid ""
msgstr ""
"Project-Id-Version: akonadi_singlefile_resource\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-08 00:48+0000\n"
"PO-Revision-Date: 2021-10-11 14:40+0300\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-POT-Import-Date: 2012-12-01 22:24:49+0000\n"
"X-Generator: Lokalize 20.04.2\n"

#. i18n: ectx: property (toolTip), widget (QTabWidget, tabWidget)
#: settingsdialog.ui:29
#, kde-format
msgid "Directory settings of the resource."
msgstr "Resurssin kansioasetukset."

#. i18n: ectx: property (whatsThis), widget (QTabWidget, tabWidget)
#: settingsdialog.ui:32
#, kde-format
msgid ""
"Select settings of the directory whose contents should be represented by "
"this resource."
msgstr "Aseta asetukset kansiolle, jonka sisältöä tämä tietolähde edustaa."

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: settingsdialog.ui:39
#, kde-format
msgid "Directory"
msgstr "Kansio"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: settingsdialog.ui:45
#, kde-format
msgid "Directory Name"
msgstr "Kansion nimi"

#. i18n: ectx: property (text), widget (QLabel, label)
#: settingsdialog.ui:53
#, kde-format
msgid "Director&y:"
msgstr "&Kansio:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: settingsdialog.ui:68
#, kde-format
msgid ""
"Select the directory whose contents should be represented by this resource. "
"If the directory does not exist, it will be created."
msgstr ""
"Valitse kansio, jonka sisältöä tämä tietolähde edustaa. Ellei kansiota ole "
"olemassa, se luodaan."

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: settingsdialog.ui:81
#, kde-format
msgid "Access Rights"
msgstr "Käyttöoikeudet"

#. i18n: ectx: property (toolTip), widget (QCheckBox, kcfg_ReadOnly)
#: settingsdialog.ui:87
#, kde-format
msgid ""
"If read-only mode is enabled, no changes will be written to the directory "
"selected above."
msgstr ""
"Jos vain luku -tila on käytössä, muutoksia ei kirjoiteta yllä valittuun "
"kansioon."

#. i18n: ectx: property (whatsThis), widget (QCheckBox, kcfg_ReadOnly)
#. i18n: ectx: property (text), widget (QLabel, label_2)
#: settingsdialog.ui:90 settingsdialog.ui:100
#, kde-format
msgid ""
"If read-only mode is enabled, no changes will be written to the directory "
"selected above. Read-only mode will be automatically enabled if you do not "
"have write access to the directory."
msgstr ""
"Jos vain luku -tila on käytössä, muutoksia ei kirjoiteta yllä valittuun "
"kansioon. Vain luku -tila otetaan käyttöön automaattisesti, ellei sinulla "
"ole kirjoitusoikeuksia kansioon."

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_ReadOnly)
#: settingsdialog.ui:93 singlefileresourceconfigwidget_desktop.ui:92
#, kde-format
msgid "Read only"
msgstr "Vain luku"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: settingsdialog.ui:127
#, kde-format
msgid "Tuning"
msgstr "Hienosäätö"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: settingsdialog.ui:133
#, kde-format
msgid ""
"The options on this page allow you to change parameters that balance data "
"safety and consistency against performance. In general you should be careful "
"with changing anything here, the defaults are good enough in most cases."
msgstr ""
"Tämän sivun valinnat sallivat sinun muuttaa asetuksia, jotka vaikuttavat "
"tietoturvan ja tiedon yhtäpitävyyden sekä suorituskyvyn tasapainoon. "
"Yleisesti ottaen sinun tulisi olla varovainen muutoksissa, koska "
"oletusasetukset ovat riittävän hyvät useimpiin tapauksiin."

#. i18n: ectx: property (text), widget (QLabel, autosaveLabel)
#: settingsdialog.ui:145
#, kde-format
msgid "Autosave delay:"
msgstr "Automaattitallennuksen viive:"

#. i18n: ectx: property (toolTip), widget (KPluralHandlingSpinBox, kcfg_AutosaveInterval)
#: settingsdialog.ui:152
#, kde-format
msgid "Autosave interval time (in minutes)."
msgstr "Automaattitallennusväli (minuutteina)."

#. i18n: ectx: property (whatsThis), widget (KPluralHandlingSpinBox, kcfg_AutosaveInterval)
#: settingsdialog.ui:155
#, kde-format
msgid "Select autosave interval time for the resource data (in minutes)."
msgstr "Aseta tietolähteen automaattitallennusväli (minuutteina)."

#: singlefileresource.h:60
#, kde-format
msgid "No file selected."
msgstr "Tiedostoa ei ole valittu."

#: singlefileresource.h:62
#, kde-format
msgid "The resource not configured yet"
msgstr "Resurssia ei ole vielä määritetty"

#: singlefileresource.h:93 singlefileresource.h:122 singlefileresource.h:207
#: singlefileresourcebase.cpp:277 singlefileresourcebase.cpp:294
#, kde-format
msgctxt "@info:status"
msgid "Ready"
msgstr "Valmis"

#: singlefileresource.h:95
#, kde-format
msgid "Could not create file '%1'."
msgstr "Tiedostoa ”%1” ei voitu luoda."

#: singlefileresource.h:109
#, kde-format
msgid "Could not read file '%1'"
msgstr "Tiedostoa ”%1” ei voitu lukea."

#: singlefileresource.h:125
#, kde-format
msgid "Another download is still in progress."
msgstr "Toinen lataus on yhä käynnissä."

#: singlefileresource.h:135 singlefileresource.h:221
#, kde-format
msgid "Another file upload is still in progress."
msgstr "Toinen tiedoston lähetys on yhä käynnissä."

#: singlefileresource.h:151
#, kde-format
msgid "Downloading remote file."
msgstr "Ladataan etätiedostoa."

#: singlefileresource.h:171
#, kde-format
msgid "Trying to write to a read-only file: '%1'."
msgstr "Yritetään kirjoittaa vain luku -tiedostoon: ”%1”."

#: singlefileresource.h:183
#, kde-format
msgid "No file specified."
msgstr "Tiedostoa ei ole määritetty."

#: singlefileresource.h:211
#, kde-format
msgid "A download is still in progress."
msgstr "Lataus on yhä käynnissä."

#: singlefileresource.h:251
#, kde-format
msgid "Uploading cached file to remote location."
msgstr "Lähetetään välimuistissa olevaa tiedostoa etäsijaintiin."

#: singlefileresourcebase.cpp:240
#, kde-format
msgid ""
"The file '%1' was changed on disk. As a precaution, a backup of its previous "
"contents has been created at '%2'."
msgstr ""
"Tiedosto ”%1” muuttui levyllä. Varotoimenpiteenä varmuuskopio sen aiemmasta "
"sisällöstä luotiin kohteeseen ”%2”."

#: singlefileresourcebase.cpp:264
#, kde-format
msgid "Could not load file '%1'."
msgstr "Tiedostoa ”%1” ei voi ladata."

#: singlefileresourcebase.cpp:283
#, kde-format
msgid "Could not save file '%1'."
msgstr "Tiedostoa ”%1” ei voi tallentaa."

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: singlefileresourceconfigwidget_desktop.ui:39
#, kde-format
msgid "File"
msgstr "Tiedosto"

#. i18n: ectx: property (text), widget (QLabel, filenameLabel)
#: singlefileresourceconfigwidget_desktop.ui:47
#, kde-format
msgid "File Name:"
msgstr "Tiedostonimi:"

#. i18n: ectx: property (text), widget (QLabel, pathLabel)
#: singlefileresourceconfigwidget_desktop.ui:66
#, kde-format
msgid ""
"Select a file. A new file will be created if it doesn't exist. You can also "
"use a URL, but in that case, monitoring for file changes is not possible."
msgstr ""
"Valitse tiedosto. Ellei sitä ole olemassa, se luodaan. Myös verkko-osoitetta "
"voi käyttää, mutta silloin tiedoston muutosten valvonta ei ole mahdollista."

#. i18n: ectx: property (text), widget (QLabel, displayNameLabel)
#: singlefileresourceconfigwidget_desktop.ui:79
#, kde-format
msgid "Display Name:"
msgstr "Näyttönimi:"

#. i18n: ectx: property (text), widget (QLabel, readOnlyLabel)
#: singlefileresourceconfigwidget_desktop.ui:108
#, kde-format
msgid ""
"When read-only mode is enabled, no changes will be written. Read-only mode "
"is automatically enabled when the selected file does not support write "
"access."
msgstr ""
"Jos vain luku -tila on käytössä, muutoksia ei kirjoiteta. Vain luku -tila "
"otetaan käyttöön automaattisesti, jos valittuun tiedostoon ei voida "
"kirjoittaa."

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_MonitorFile)
#: singlefileresourceconfigwidget_desktop.ui:121
#, kde-format
msgid "Enable file &monitoring"
msgstr "T&arkkaile tiedostoja"

#. i18n: ectx: property (text), widget (QLabel, monitoringLabel)
#: singlefileresourceconfigwidget_desktop.ui:134
#, kde-format
msgid ""
"Changes made by other applications will reload the file when monitoring is "
"enabled. Backups will be created to avoid conflicts when possible."
msgstr ""
"Valvonnan ollessa käytössä muiden sovellusten tekemät muutokset saavat "
"tiedoston latautumaan uudelleen. Ristiriitojen välttämiseksi tehdään "
"varmuuskopio aina kun mahdollista."

#: singlefileresourceconfigwidgetbase.cpp:126
#, kde-format
msgctxt "@info:status"
msgid "Checking file information..."
msgstr "Tarkistetaan tiedoston tietoja…"

#~ msgid "Filename"
#~ msgstr "Tiedostonimi"

#~ msgid "Status:"
#~ msgstr "Tila:"

#~ msgid ""
#~ "Select the file whose contents should be represented by this resource. If "
#~ "the file does not exist, it will be created. A URL of a remote file can "
#~ "also be specified, but note that monitoring for file changes will not "
#~ "work in this case."
#~ msgstr ""
#~ "Valitse tiedosto, jonka sisältöä tämä tietolähde edustaa. Ellei tiedostoa "
#~ "ole olemassa, se luodaan. Myös verkko-osoite tai etätiedosto voidaan "
#~ "määrittää, mutta tällöin tiedoston muutosten valvonta ei ole "
#~ "käytettävissä."

#~ msgid "&Name:"
#~ msgstr "&Nimi:"

#~ msgid ""
#~ "Enter the name used to identify this resource in displays. If not "
#~ "specified, the filename will be used."
#~ msgstr ""
#~ "Anna nimi, jolla tämä resurssi esitetään eri näkymissä. Ellei annettu, "
#~ "käytetään tiedostonimeä."

#~ msgid "Monitoring"
#~ msgstr "Tarkkailu"

#~ msgid ""
#~ "If file monitoring is enabled the resource will reload the file when "
#~ "changes are made by other programs. It also tries to create a backup in "
#~ "case of conflicts whenever possible."
#~ msgstr ""
#~ "Jos tarkkailu on käytössä, tietolähde lataa tiedoston uudelleen muiden "
#~ "ohjelmien muutettua sitä. Se yrittää myös luoda varmuuskopion "
#~ "ristiriitojen varalta aina kun mahdollista."

#~ msgid ""
#~ "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
#~ "REC-html40/strict.dtd\">\n"
#~ "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/"
#~ "css\">\n"
#~ "p, li { white-space: pre-wrap; }\n"
#~ "</style></head><body style=\" font-family:'Noto Sans'; font-size:10pt; "
#~ "font-weight:400; font-style:normal;\">\n"
#~ "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#~ "right:0px; -qt-block-indent:0; text-indent:0px;\">Select the file whose "
#~ "contents should be represented by this resource. If the file does not "
#~ "exist, it will be created. A URL of a remote file can also be specified, "
#~ "but note that monitoring for file changes will not work in this case.</"
#~ "p></body></html>"
#~ msgstr ""
#~ "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
#~ "REC-html40/strict.dtd\">\n"
#~ "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/"
#~ "css\">\n"
#~ "p, li { white-space: pre-wrap; }\n"
#~ "</style></head><body style=\" font-family:'Noto Sans'; font-size:10pt; "
#~ "font-weight:400; font-style:normal;\">\n"
#~ "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#~ "right:0px; -qt-block-indent:0; text-indent:0px;\">Valitse tiedosto, jonka "
#~ "sisältöä tämän resurssin tulisi edustaa. Ellei tiedostoa ole olemassa, se "
#~ "luodaan. Myös etätiedoston verkko-osoitteen voi antaa, mutta tiedoston "
#~ "muutosten valvonta ei tällöin toimi.</p></body></html>"

#~ msgid ""
#~ "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
#~ "REC-html40/strict.dtd\">\n"
#~ "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/"
#~ "css\">\n"
#~ "p, li { white-space: pre-wrap; }\n"
#~ "</style></head><body style=\" font-family:'Noto Sans'; font-size:10pt; "
#~ "font-weight:400; font-style:normal;\">\n"
#~ "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#~ "right:0px; -qt-block-indent:0; text-indent:0px;\">Enter the name used to "
#~ "identify this resource in displays. If not specified, the filename will "
#~ "be used.</p></body></html>"
#~ msgstr ""
#~ "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
#~ "REC-html40/strict.dtd\">\n"
#~ "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/"
#~ "css\">\n"
#~ "p, li { white-space: pre-wrap; }\n"
#~ "</style></head><body style=\" font-family:'Noto Sans'; font-size:10pt; "
#~ "font-weight:400; font-style:normal;\">\n"
#~ "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#~ "right:0px; -qt-block-indent:0; text-indent:0px;\">Anna nimi, jolla "
#~ "resurssi esitetään eri näkymissä. Ellei anneta, käytetään tiedostonimeä.</"
#~ "p></body></html>"

#~ msgid ""
#~ "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
#~ "REC-html40/strict.dtd\">\n"
#~ "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/"
#~ "css\">\n"
#~ "p, li { white-space: pre-wrap; }\n"
#~ "</style></head><body style=\" font-family:'Noto Sans'; font-size:10pt; "
#~ "font-weight:400; font-style:normal;\">\n"
#~ "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#~ "right:0px; -qt-block-indent:0; text-indent:0px;\">If read-only mode is "
#~ "enabled, no changes will be written to the file selected above. Read-only "
#~ "mode will be automatically enabled if you do not have write access to the "
#~ "file or the file is on a remote server that does not support write access."
#~ "</p></body></html>"
#~ msgstr ""
#~ "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
#~ "REC-html40/strict.dtd\">\n"
#~ "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/"
#~ "css\">\n"
#~ "p, li { white-space: pre-wrap; }\n"
#~ "</style></head><body style=\" font-family:'Noto Sans'; font-size:10pt; "
#~ "font-weight:400; font-style:normal;\">\n"
#~ "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#~ "right:0px; -qt-block-indent:0; text-indent:0px;\">Jos vain luku -tila "
#~ "otetaan käyttöön, muutoksia ei kirjoiteta yllä valittuun tiedostoon. Vain "
#~ "luku -tila valitaan automaattisesti, jollei sinulla ole "
#~ "kirjoitusoikeuksia tiedostoon tai tiedosto sijaitsee etäpalvelimella, "
#~ "joka ei tue kirjoitusta.</p></body></html>"

#~ msgid ""
#~ "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
#~ "REC-html40/strict.dtd\">\n"
#~ "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/"
#~ "css\">\n"
#~ "p, li { white-space: pre-wrap; }\n"
#~ "</style></head><body style=\" font-family:'Noto Sans'; font-size:10pt; "
#~ "font-weight:400; font-style:normal;\">\n"
#~ "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#~ "right:0px; -qt-block-indent:0; text-indent:0px;\">If file monitoring is "
#~ "enabled the resource will reload the file when changes are made by other "
#~ "programs. It also tries to create a backup in case of conflicts whenever "
#~ "possible.</p></body></html>"
#~ msgstr ""
#~ "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
#~ "REC-html40/strict.dtd\">\n"
#~ "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/"
#~ "css\">\n"
#~ "p, li { white-space: pre-wrap; }\n"
#~ "</style></head><body style=\" font-family:'Noto Sans'; font-size:10pt; "
#~ "font-weight:400; font-style:normal;\">\n"
#~ "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#~ "right:0px; -qt-block-indent:0; text-indent:0px;\">Jos tiedoston valvonta "
#~ "otetaan käyttöön, resurssi lataa tiedoston uudelleen, jos muut ohjelmat "
#~ "muuttavat sitä. Ristiriitatilanteissa yritetään myös luoda varmuuskopio "
#~ "aina kun mahdollista.</p></body></html>"

#~ msgid "Could not find account"
#~ msgstr "Tiliä ei löydy"

#~ msgid "Could not find credentials"
#~ msgstr "Tunnistautumistietoja ei löydy"

#~ msgid "&Display name:"
#~ msgstr "&Näyttönimi:"

# Tämän edellä on kirjoitushetkellä ”Autosave delay:”
#~ msgid " minute"
#~ msgid_plural " minutes"
#~ msgstr[0] " minuutti"
#~ msgstr[1] " minuuttia"
