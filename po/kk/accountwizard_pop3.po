# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sairan Kikkarin <sairan(at)computer.org>, 2010, 2013.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-08-15 02:21+0200\n"
"PO-Revision-Date: 2013-11-18 04:29+0600\n"
"Last-Translator: Sairan Kikkarin <sairan@computer.org>\n"
"Language-Team: Kazakh <kde-i18n-doc@kde.org>\n"
"Language: kk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: pop3wizard.es:10
msgid "Personal Settings"
msgstr "Дербес параметрлері"

#. i18n: ectx: property (text), widget (QLabel, label)
#: pop3wizard.ui:21
#, kde-format
msgid "Username:"
msgstr "Пайдаланушы:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: pop3wizard.ui:28
#, kde-format
msgid "Incoming server:"
msgstr "Кіріс пошта сервері:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: pop3wizard.ui:38
#, kde-format
msgid "Outgoing server:"
msgstr "Шығыс пошта сервері:"
