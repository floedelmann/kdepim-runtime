# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sergiu Bivol <sergiu@cip.md>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-08-15 02:21+0200\n"
"PO-Revision-Date: 2014-10-03 10:59+0300\n"
"Last-Translator: Sergiu Bivol <sergiu@ase.md>\n"
"Language-Team: Romanian <kde-i18n-ro@kde.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"

#: vcarddirwizard.es.cmake:7
msgid "Settings"
msgstr "Configurări"

#: vcarddirwizard.es.cmake:24
msgid "Default Contact"
msgstr "Contact implicit"

#. i18n: ectx: property (text), widget (QLabel, label)
#: vcarddirwizard.ui:19
#, kde-format
msgid "Path:"
msgstr "Cale:"
