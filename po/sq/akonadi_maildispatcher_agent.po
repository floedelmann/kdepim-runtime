# Albanian translation for kdepim-runtime
# Copyright (c) 2010 Rosetta Contributors and Canonical Ltd 2010
# This file is distributed under the same license as the kdepim-runtime package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: kdepim-runtime\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-13 00:46+0000\n"
"PO-Revision-Date: 2010-03-03 13:49+0000\n"
"Last-Translator: Vilson Gjeci <vilsongjeci@gmail.com>\n"
"Language-Team: Albanian <sq@li.org>\n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Launchpad-Export-Date: 2011-04-22 01:26+0000\n"
"X-Generator: Launchpad (build 12883)\n"

#: maildispatcheragent.cpp:84 maildispatcheragent.cpp:233
#, kde-format
msgid "Sending messages (1 item in queue)..."
msgid_plural "Sending messages (%1 items in queue)..."
msgstr[0] ""
msgstr[1] ""

#: maildispatcheragent.cpp:94
#, kde-format
msgid "Sending canceled."
msgstr ""

#: maildispatcheragent.cpp:101
#, kde-format
msgid "Finished sending messages."
msgstr ""

#: maildispatcheragent.cpp:107
#, kde-format
msgctxt "Notification title when email was sent"
msgid "E-Mail Successfully Sent"
msgstr ""

#: maildispatcheragent.cpp:108
#, kde-format
msgctxt "Notification when the email was sent"
msgid "Your E-Mail has been sent successfully."
msgstr ""

#: maildispatcheragent.cpp:114
#, kde-format
msgid "No items in queue."
msgstr ""

#: maildispatcheragent.cpp:164
#, kde-format
msgid "Online, sending messages in queue."
msgstr ""

#: maildispatcheragent.cpp:168
#, kde-format
msgid "Offline, message sending suspended."
msgstr ""

#: maildispatcheragent.cpp:193
#, kde-format
msgctxt "Message with given subject is being sent."
msgid "Sending: %1"
msgstr ""

#: maildispatcheragent.cpp:258
#, kde-format
msgctxt "Notification title when email sending failed"
msgid "E-Mail Sending Failed"
msgstr ""

#: maildispatcheragent.cpp:291
#, kde-format
msgid "Ready to dispatch messages."
msgstr ""

#. i18n: ectx: label, entry (Outbox), group (General)
#: maildispatcheragent.kcfg:10
#, kde-format
msgid "Outbox collection id"
msgstr ""

#. i18n: ectx: label, entry (SentMail), group (General)
#: maildispatcheragent.kcfg:14
#, kde-format
msgid "Sent Mail collection id"
msgstr ""

#: outboxqueue.cpp:265
#, kde-format
msgid "Could not access the outbox folder (%1)."
msgstr ""

#: sendjob.cpp:49 sendjob.cpp:50
#, kde-format
msgid "Message sending aborted."
msgstr ""

#: sendjob.cpp:59
#, kde-format
msgid "Could not initiate message transport. Possibly invalid transport."
msgstr ""

#: sendjob.cpp:65
#, kde-format
msgid "Could not send message. Invalid transport."
msgstr ""

#: sendjob.cpp:97
#, kde-format
msgid "Failed to get D-Bus interface of resource %1."
msgstr ""

#: sendjob.cpp:110
#, kde-format
msgid "Invalid D-Bus reply from resource %1."
msgstr ""

#: sendjob.cpp:228
#, kde-format
msgid "Default sent-mail folder unavailable. Keeping message in outbox."
msgstr ""

#: sendjob.cpp:240
#, kde-format
msgid "Message transport aborted."
msgstr ""

#: sendjob.cpp:240
#, kde-format
msgid "Failed to transport message."
msgstr ""

#: sendjob.cpp:282
#, kde-format
msgid "Failed to get D-Bus interface of mailfilteragent."
msgstr ""

#: sendjob.cpp:289
#, kde-format
msgid "Invalid D-Bus reply from mailfilteragent"
msgstr ""

#: sendjob.cpp:331
#, kde-format
msgid "Sending succeeded, but failed to remove the message from the outbox."
msgstr ""

#: sendjob.cpp:334
#, kde-format
msgid ""
"Sending succeeded, but failed to move the message to the sent-mail folder."
msgstr ""

#: sendjob.cpp:370
#, kde-format
msgid "Failed to store result in item."
msgstr ""

#: storeresultjob.cpp:60
#, kde-format
msgid "Failed to fetch item."
msgstr ""

#~ msgid "Mail Dispatcher Agent Settings"
#~ msgstr "Parametrat e Agjentit Shpërndarës së Postës"
