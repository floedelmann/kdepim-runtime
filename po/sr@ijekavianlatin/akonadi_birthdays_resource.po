# Translation of akonadi_birthdays_resource.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2011, 2016.
msgid ""
msgstr ""
"Project-Id-Version: akonadi_birthdays_resource\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-06-21 03:22+0200\n"
"PO-Revision-Date: 2016-05-22 23:05+0200\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@ijekavianlatin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#: birthdaysconfigagentwidget.cpp:47
#, kde-format
msgid " day"
msgid_plural " days"
msgstr[0] " dan"
msgstr[1] " dana"
msgstr[2] " dana"
msgstr[3] " dan"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: birthdaysconfigwidget.ui:29
#, kde-format
msgid "Reminder"
msgstr "Podsjetnik"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_EnableAlarm)
#: birthdaysconfigwidget.ui:38
#, kde-format
msgid "Set &reminder"
msgstr "&Postavi podsjetnik"

#. i18n: ectx: property (text), widget (QLabel, label)
#: birthdaysconfigwidget.ui:48
#, kde-format
msgid "&Remind prior to event:"
msgstr "Podsjeti pred &događaj:"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: birthdaysconfigwidget.ui:74
#, kde-format
msgid "Filter"
msgstr "Filter"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_FilterOnCategories)
#: birthdaysconfigwidget.ui:80
#, kde-format
msgid "&Filter by categories"
msgstr "&Filter po kategorijama"

#: birthdaysresource.cpp:54
#, kde-format
msgid "Birthdays & Anniversaries"
msgstr "Rođendani i godišnjice"

#: birthdaysresource.cpp:240
#, kde-format
msgid "%1's birthday"
msgstr "Rođendan — %1"

#: birthdaysresource.cpp:251
#, kde-format
msgid "Birthday"
msgstr "Rođendan"

#: birthdaysresource.cpp:295
#, kde-format
msgctxt "insert names of both spouses"
msgid "%1's & %2's anniversary"
msgstr "Godišnjica — %1 i %2"

#: birthdaysresource.cpp:298
#, kde-format
msgctxt "only one spouse in addressbook, insert the name"
msgid "%1's anniversary"
msgstr "%1 — godišnjica"

#: birthdaysresource.cpp:310
#, kde-format
msgid "Anniversary"
msgstr "Godišnjica"
