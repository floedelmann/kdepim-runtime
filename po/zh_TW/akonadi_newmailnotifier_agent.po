# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Frank Weng (a.k.a. Franklin) <franklin@goodhorse.idv.tw>, 2011.
# Franklin Weng <franklin at goodhorse dot idv dot tw>, 2013, 2014, 2015.
# Jeff Huang <s8321414@gmail.com>, 2016, 2017.
# pan93412 <pan93412@gmail.com>, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-13 00:46+0000\n"
"PO-Revision-Date: 2019-01-19 19:35+0800\n"
"Last-Translator: pan93412 <pan93412@gmail.com>\n"
"Language-Team: Chinese <zh-l10n@lists.linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 18.12.1\n"

#: newmailnotifieragent.cpp:339
#, kde-format
msgctxt ""
"%2 = name of mail folder; %3 = name of Akonadi POP3/IMAP/etc resource (as "
"user named it)"
msgid "One new email in %2 from \"%3\""
msgid_plural "%1 new emails in %2 from \"%3\""
msgstr[0] "在 %2 中有 \"%3\" 來的 %1 封新信件"

#: newmailnotifieragent.cpp:359
#, kde-format
msgid "New mail arrived"
msgstr "新郵件抵達"

#: newmailnotifieragentsettings.kcfg:34
#, kde-format
msgctxt "%f is a variable for agent. Do not change it"
msgid "A message was received from %f"
msgstr "已從 %f 取得信件"

#: newmailnotifierreplymessagejob.cpp:38 newmailnotifiershowmessagejob.cpp:38
#, kde-format
msgid "Unable to start KMail application."
msgstr "無法啟動 KMail 應用程式。"

#: newmailnotifierselectcollectionwidget.cpp:94
#, kde-format
msgid "Select which folders to monitor for new message notifications:"
msgstr "選擇新信件通知監視的資料夾："

#: newmailnotifierselectcollectionwidget.cpp:125
#, kde-format
msgid "Search..."
msgstr "搜尋..."

#: newmailnotifierselectcollectionwidget.cpp:140
#, kde-format
msgid "&Select All"
msgstr "全部選取(&S)"

#: newmailnotifierselectcollectionwidget.cpp:144
#, kde-format
msgid "&Unselect All"
msgstr "全部取消選取(&U)"

#: newmailnotifiersettingswidget.cpp:31
#, c-format
msgid ""
"<qt><p>Here you can define message. You can use:</p><ul><li>%s set subject</"
"li><li>%f set from</li></ul></qt>"
msgstr ""
"<qt><p>您可以在此定義信件。您可以用：</p><ul><li>%s 設定主旨</li><li>%f 設定"
"寄件者</li></ul></qt>"

#: newmailnotifiersettingswidget.cpp:53
#, kde-format
msgid "Choose which fields to show:"
msgstr "選擇要顯示哪些欄位："

#: newmailnotifiersettingswidget.cpp:58
#, kde-format
msgid "Show Photo"
msgstr "顯示相片"

#: newmailnotifiersettingswidget.cpp:62
#, kde-format
msgid "Show From"
msgstr "顯示寄件者"

#: newmailnotifiersettingswidget.cpp:66
#, kde-format
msgid "Show Subject"
msgstr "顯示主旨"

#: newmailnotifiersettingswidget.cpp:70
#, kde-format
msgid "Show Folders"
msgstr "顯示資料夾"

#: newmailnotifiersettingswidget.cpp:74
#, kde-format
msgid "Do not notify when email was sent by me"
msgstr "我寄出的郵件不用通知"

#: newmailnotifiersettingswidget.cpp:78
#, kde-format
msgid "Keep Persistent Notification"
msgstr "保持持續通知"

#: newmailnotifiersettingswidget.cpp:82
#, kde-format
msgid "Show Action Buttons"
msgstr "顯示動作按鈕"

#: newmailnotifiersettingswidget.cpp:90
#, kde-format
msgid "Reply Mail"
msgstr ""

#: newmailnotifiersettingswidget.cpp:98 specialnotifierjob.cpp:154
#, kde-format
msgid "Reply to Author"
msgstr ""

#: newmailnotifiersettingswidget.cpp:98 specialnotifierjob.cpp:157
#, fuzzy, kde-format
#| msgid "&Select All"
msgid "Reply to All"
msgstr "全部選取(&S)"

#: newmailnotifiersettingswidget.cpp:107
#, kde-format
msgid "Display"
msgstr "顯示"

#: newmailnotifiersettingswidget.cpp:112
#, kde-format
msgid "Enabled"
msgstr "已開啟"

#: newmailnotifiersettingswidget.cpp:116
#, kde-format
msgid "<a href=\"whatsthis\">How does this work?</a>"
msgstr "<a href=\"whatsthis\">這是怎麼運作的？</a>"

#: newmailnotifiersettingswidget.cpp:125
#, kde-format
msgid "Message:"
msgstr "訊息："

#: newmailnotifiersettingswidget.cpp:136
#, kde-format
msgid "Text to Speak"
msgstr "要唸的文字"

#: newmailnotifiersettingswidget.cpp:142
#, kde-format
msgid "Notify"
msgstr "通知"

#: newmailnotifiersettingswidget.cpp:145
#, kde-format
msgid "Folders"
msgstr "資料夾"

#: newmailnotifiersettingswidget.cpp:148
#, kde-format
msgid "New Mail Notifier Agent"
msgstr "新信件通知代理程式"

#: newmailnotifiersettingswidget.cpp:150
#, kde-format
msgid "Notify about new mails."
msgstr "新郵件通知。"

#: newmailnotifiersettingswidget.cpp:152
#, fuzzy, kde-format
#| msgid "Copyright (C) 2013-2019 Laurent Montel"
msgid "Copyright (C) 2013-%1 Laurent Montel"
msgstr "Copyright (C) 2013-2019 Laurent Montel"

#: newmailnotifiersettingswidget.cpp:155
#, kde-format
msgid "Laurent Montel"
msgstr "Laurent Montel"

#: newmailnotifiersettingswidget.cpp:155
#, kde-format
msgid "Maintainer"
msgstr "維護者"

#: newmailnotifiersettingswidget.cpp:156
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Franklin Weng"

#: newmailnotifiersettingswidget.cpp:156
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "franklin@nospam.goodhorse.idv.tw"

#: specialnotifierjob.cpp:116
#, kde-format
msgid "From: %1"
msgstr "寄件者：%1"

#: specialnotifierjob.cpp:124
#, kde-format
msgid "Subject: %1"
msgstr "主旨：%1"

#: specialnotifierjob.cpp:127
#, kde-format
msgid "In: %1"
msgstr "於：%1"

#: specialnotifierjob.cpp:150
#, kde-format
msgid "Show mail..."
msgstr "顯示信件..."

#: specialnotifierjob.cpp:150
#, kde-format
msgid "Mark As Read"
msgstr "標記為已讀"

#: specialnotifierjob.cpp:150
#, kde-format
msgid "Delete"
msgstr "刪除"
